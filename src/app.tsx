import * as React from 'react'
import { createRoot } from 'react-dom/client'
import { AppProvider, useAppContext } from './AppContext'

const AuthedApp = React.lazy(() => import('./AuthedApp'))
const UnauthedApp = React.lazy(() => import('./UnauthedApp'))

const App = () => {
  const { user } = useAppContext()
  return user ? <AuthedApp /> : <UnauthedApp /> 
}

createRoot(document.getElementById('root')!)
  .render(
    <AppProvider>
      <App />
    </AppProvider>
  )